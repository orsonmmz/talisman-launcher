# Published via https://hub.docker.com/r/orsonmmz/mxe-wxwidgets

FROM alpine:3.14

RUN apk add --no-cache \
    autoconf automake bash binutils bison bzip2 flex g++ gdk-pixbuf gettext \
    git gperf intltool libtool linux-headers lzip make openssl openssl-dev \
    p7zip patch perl python3 py3-mako ruby unzip wget xz zlib
RUN ln -s /usr/bin/python3 /usr/bin/python
RUN git clone --depth=1 https://github.com/mxe/mxe.git
RUN make -C mxe wxwidgets