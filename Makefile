STATIC ?= n
DEBUG ?= n

CC=$(CROSS)gcc
CXX=$(CROSS)g++
LD=$(CROSS)ld
AR=$(CROSS)ar
PKG_CONFIG=$(CROSS)pkg-config
WX_CONFIG=$(CROSS)wx-config

ifeq ($(DEBUG),y)
       CXXFLAGS += `${WX_CONFIG} --cxxflags --debug=yes` -O0 -g
else
       CXXFLAGS += `${WX_CONFIG} --cxxflags --debug=no` -O2
endif

ifeq ($(STATIC),y)
       LDFLAGS += `${WX_CONFIG} --libs --static=yes` -static
else
       LDFLAGS += `${WX_CONFIG} --libs`
endif


all: talisman_launcher.exe

%.o: %.cpp
	$(CXX) -c $(CXXFLAGS) $^ -o $@

talisman_launcher.exe: main.o ui.o
	$(CXX) $^ -o $@ $(LDFLAGS)


clean:
	rm *.o || true

.PHONY: clean
