/**
 * Copyright (C) 2022 Maciej Suminski <orson@orson.net.pl>
 *
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option)
 * any later version.

 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.

 * You should have received a copy of the GNU General Public License along
 * with this program. If not, see <https://www.gnu.org/licenses/>. 
 */

#ifndef _UI_H
#define _UI_H

#include <wx/wx.h>
 
class MainFrame : public wxFrame
{
public:
    MainFrame();
 
private:
    static int runGame(bool blocking = true);

    void refreshGames();
    void updateButtons();
    wxString selectedGame() const;
    wxString askGameName(const wxString& defaultName);
    void log(const wxString& msg);

    static wxString getSaveDirTop();
    static wxString getSaveDir(const wxString& game);

    static wxString gameToDirectory(const wxString& game);
    static wxString directoryToGame(const wxString& dir);

    static bool currentGameAvailable();

    static int copyDirectory(const wxString& sourcePath, const wxString& targetPath);
    static int moveDirectory(const wxString& sourcePath, const wxString& targetPath);
    static int moveCurrentToGame(const wxString& targetGame);
    static int moveGameToCurrent(const wxString& sourceGame);
    int backupCurrent();
    int restoreCurrent();

    // Event handlers
    void onStartNewClick(wxCommandEvent& event);
    void onStartSelectedClick(wxCommandEvent& event);
    void onCopyClick(wxCommandEvent& event);
    void onRenameClick(wxCommandEvent& event);
    void onDeleteClick(wxCommandEvent& event);
    void onOpenSaveDirClick(wxCommandEvent& event);
    void onCloseClick(wxCommandEvent& event);
    void onGameSelected(wxCommandEvent& event);
    void onGameStarted(wxThreadEvent& event);

    wxPanel* panelMain;
    wxListBox* lstGames;
    wxButton* btnStartNew;
    wxButton* btnStartSelected;
    wxButton* btnCopy;
    wxButton* btnRename;
    wxButton* btnDelete;
    wxButton* btnOpenSaveDir;
    wxButton* btnClose;
    wxTextCtrl* txtLog;

    wxString backupName;
};
 
#endif // _UI_H
