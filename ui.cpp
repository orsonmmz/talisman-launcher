/**
 * Copyright (C) 2022 Maciej Suminski <orson@orson.net.pl>
 *
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option)
 * any later version.

 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.

 * You should have received a copy of the GNU General Public License along
 * with this program. If not, see <https://www.gnu.org/licenses/>. 
 */

#include "ui.h"

#include <wx/dir.h>
#include <wx/filename.h>
#include <wx/stdpaths.h>

// Location of the game configuration directory
static const wxString TALISMAN_DIR("\\Nomad Games\\Talisman");

// Name of the save game directory (also used as a prefix for the save copies)
static const wxString SAVE_DIR("saved_game");

// String representing the current save game
static const wxString CURRENT_GAME("[current game]");

// Game window title
static constexpr char TALISMAN_WINDOW[] = "Talisman Digital Edition";


enum
{
    ID_LST_GAMES,
    ID_BTN_START_NEW,
    ID_BTN_START_SELECTED,
    ID_BTN_COPY,
    ID_BTN_RENAME,
    ID_BTN_DELETE,
    ID_BTN_OPEN_SAVE_DIR,
    ID_BTN_CLOSE,
    ID_GAME_STARTED,
};


static bool isGameRunning()
{
    return FindWindowA(nullptr, TALISMAN_WINDOW) != 0;
}


class DialogGameStart : public wxDialog
{
public:
    DialogGameStart(wxWindow* parent)
        : wxDialog(parent, wxID_ANY, wxT("Starting Talisman"))
    {
        wxSizer* szrMessage = CreateTextSizer(wxT("Waiting for the game to start..."));
        wxSizer* szrButtons = CreateStdDialogButtonSizer(wxCANCEL);

        wxBoxSizer* szrMain = new wxBoxSizer(wxVERTICAL);
        szrMain->Add(szrMessage, 1, wxEXPAND | wxALL, 20);
        szrMain->Add(szrButtons, 0, wxEXPAND | wxALL, 20);
        SetSizer(szrMain);

        Bind(wxEVT_THREAD, &DialogGameStart::onGameStarted, this, ID_GAME_STARTED);
    }

private:
    void onGameStarted(wxEvent& event)
    {
        if (IsModal())
        {
            EndModal(wxID_OK);
        }
    }
};


class WaitForGame : public wxThread
{
public:
    WaitForGame(wxEvtHandler* eventHandler)
        : wxThread(wxTHREAD_JOINABLE), evtHandler(eventHandler)
    {
    }

    ExitCode Entry() override
    {
        while (not isGameRunning())
        {
            wxMilliSleep(250);

            if (TestDestroy())
            {
                return (wxThread::ExitCode) 1;
            }
        }

        wxQueueEvent(evtHandler, new wxThreadEvent(wxEVT_THREAD, ID_GAME_STARTED));
        return 0;
    }

private:
    wxEvtHandler* evtHandler;
};

 
MainFrame::MainFrame()
    : wxFrame(nullptr, wxID_ANY, "Talisman Launcher 0.3alpha", wxDefaultPosition, wxSize(400, 600))
{
    // Layout
    panelMain = new wxPanel(this, wxID_ANY);

    lstGames = new wxListBox(panelMain, ID_LST_GAMES);

    btnStartNew = new wxButton(panelMain, ID_BTN_START_NEW, wxT("Start new"));
    btnStartSelected = new wxButton(panelMain, ID_BTN_START_SELECTED, wxT("Start selected"));
    btnCopy = new wxButton(panelMain, ID_BTN_COPY, wxT("Copy"));
    btnRename = new wxButton(panelMain, ID_BTN_RENAME, wxT("Rename"));
    btnDelete = new wxButton(panelMain, ID_BTN_DELETE, wxT("Delete"));
    btnOpenSaveDir = new wxButton(panelMain, ID_BTN_OPEN_SAVE_DIR, wxT("Open save game directory"));
    btnClose = new wxButton(panelMain, ID_BTN_CLOSE, wxT("Close"));

    wxBoxSizer* boxActions = new wxBoxSizer(wxVERTICAL);
    boxActions->Add(btnStartNew, 1, wxEXPAND | wxALL);
    boxActions->Add(btnStartSelected, 1, wxEXPAND | wxALL);
    boxActions->Add(btnCopy, 1, wxEXPAND | wxALL);
    boxActions->Add(btnRename, 1, wxEXPAND | wxALL);
    boxActions->Add(btnDelete, 1, wxEXPAND | wxALL);
    boxActions->Add(btnOpenSaveDir, 1, wxEXPAND | wxALL);
    boxActions->Add(btnClose, 1, wxEXPAND | wxALL);

    wxBoxSizer* boxTop = new wxBoxSizer(wxHORIZONTAL);
    boxTop->Add(lstGames, 2, wxEXPAND | wxALL);
    boxTop->Add(boxActions, 1, wxEXPAND | wxALL);

    txtLog = new wxTextCtrl(panelMain, wxID_ANY, wxEmptyString,
            wxDefaultPosition, wxDefaultSize, wxTE_MULTILINE | wxTE_READONLY);

    wxBoxSizer* boxMain = new wxBoxSizer(wxVERTICAL);
    boxMain->Add(boxTop, 3, wxEXPAND | wxALL);
    boxMain->Add(txtLog, 1, wxEXPAND | wxALL);
    
    panelMain->SetSizer(boxMain);

    // Event handlers
    Bind(wxEVT_COMMAND_BUTTON_CLICKED, &MainFrame::onStartNewClick,      this, ID_BTN_START_NEW);
    Bind(wxEVT_COMMAND_BUTTON_CLICKED, &MainFrame::onStartSelectedClick, this, ID_BTN_START_SELECTED);
    Bind(wxEVT_COMMAND_BUTTON_CLICKED, &MainFrame::onCopyClick,          this, ID_BTN_COPY);
    Bind(wxEVT_COMMAND_BUTTON_CLICKED, &MainFrame::onRenameClick,        this, ID_BTN_RENAME);
    Bind(wxEVT_COMMAND_BUTTON_CLICKED, &MainFrame::onDeleteClick,        this, ID_BTN_DELETE);
    Bind(wxEVT_COMMAND_BUTTON_CLICKED, &MainFrame::onOpenSaveDirClick,   this, ID_BTN_OPEN_SAVE_DIR);
    Bind(wxEVT_COMMAND_BUTTON_CLICKED, &MainFrame::onCloseClick,         this, ID_BTN_CLOSE);
    Bind(wxEVT_LISTBOX,                &MainFrame::onGameSelected,       this, ID_LST_GAMES);
    lstGames->Bind(wxEVT_LISTBOX_DCLICK, [&](wxCommandEvent& e) {
        wxQueueEvent(this, new wxCommandEvent(wxEVT_COMMAND_BUTTON_CLICKED, ID_BTN_START_SELECTED));
    });

    refreshGames();
}


int MainFrame::runGame(bool blocking)
{
    if (wxExecute("explorer \"steam://rungameid/247000\"", wxEXEC_BLOCK) == 0)
    {
        //return -1;     // TODO check
    }

    if (blocking)
    {
        DialogGameStart dlgStart(nullptr);
        WaitForGame threadWait(&dlgStart);

        threadWait.Run();
        int result = dlgStart.ShowModal();
        threadWait.Delete();

        switch (result)
        {
            case wxID_OK:
                while (isGameRunning())
                {
                    wxMilliSleep(250);
                }
                return 0;
                break;

            case wxID_CANCEL:
                return -1;
                break;

            default:
                return -2;
        }
    }

    return 0;
}


void MainFrame::refreshGames()
{
    lstGames->Clear();
    updateButtons();

    wxDir configDir(getSaveDirTop());
    wxString dirName;

    bool more = configDir.GetFirst(&dirName, wxEmptyString, wxDIR_DIRS);
    while (more)
    {
        if (dirName.StartsWith(SAVE_DIR))
        {
            lstGames->Append(directoryToGame(dirName));
        }

        more = configDir.GetNext(&dirName);
    }
}


void MainFrame::updateButtons()
{
    bool selectionEmpty = selectedGame().IsEmpty();
    btnStartNew->Enable(true);
    btnStartSelected->Enable(not selectionEmpty);
    btnCopy->Enable(not selectionEmpty);
    btnRename->Enable(not selectionEmpty);
    btnDelete->Enable(not selectionEmpty);
    btnClose->Enable(true);
}


wxString MainFrame::selectedGame() const
{
    int id = lstGames->GetSelection();

    if (id == wxNOT_FOUND)
    {
        return "";
    }

    return lstGames->GetString(id);
}


wxString MainFrame::askGameName(const wxString& defaultName)
{
    wxTextEntryDialog dlgName(nullptr, wxT("Enter game name"), wxT("Game name"), defaultName);

    if (dlgName.ShowModal() != wxID_OK)
    {
        return "";
    }

    wxString gameName = dlgName.GetValue();
    wxString gameDir = getSaveDir(gameName);

    if (wxDirExists(gameDir))
    {
        wxMessageBox(wxT("Game already exists"), wxT("Error"), wxICON_ERROR | wxOK);
        return "";
    }

    return gameName;
}


void MainFrame::log(const wxString& msg)
{
    txtLog->AppendText(msg + "\n");
}


wxString MainFrame::getSaveDirTop()
{
    return wxStandardPaths::Get().GetUserConfigDir() + TALISMAN_DIR;
}


wxString MainFrame::getSaveDir(const wxString& game)
{
    return getSaveDirTop() + "\\" + gameToDirectory(game);
}


wxString MainFrame::gameToDirectory(const wxString& game)
{
    if (game == CURRENT_GAME)
    {
        return SAVE_DIR;
    }

    // TODO validate selection string
    return SAVE_DIR + "_" + game;
}


wxString MainFrame::directoryToGame(const wxString& dir)
{
    wxASSERT(dir.StartsWith(SAVE_DIR));

    if (dir == SAVE_DIR)
    {
        return CURRENT_GAME;
    }

    wxString copy(dir);
    copy.Replace(SAVE_DIR + "_", "");
    return copy;
}


bool MainFrame::currentGameAvailable()
{
    return wxDirExists(getSaveDir(CURRENT_GAME));
}


int MainFrame::copyDirectory(const wxString& sourcePath, const wxString& targetPath)
{
    // Create the target directory
    if (not wxDir::Make(targetPath))
    {
        return -1;
    }

    // Copy the files
    wxDir sourceDir(sourcePath);
    wxString fileName;

    bool more = sourceDir.GetFirst(&fileName, wxEmptyString);
    while (more)
    {
        wxFileName sourceFile(sourcePath, fileName);
        wxFileName targetFile(targetPath, fileName);

        if (not wxCopyFile(sourceFile.GetFullPath(), targetFile.GetFullPath()))
        {
            return -2;
        }

        more = sourceDir.GetNext(&fileName);
    }

    return 0;
}


int MainFrame::moveDirectory(const wxString& sourcePath, const wxString& targetPath)
{
    if (targetPath.IsEmpty())
    {
        return -1;
    }

    if (not wxDirExists(sourcePath))
    {
        return -2;
    }
    
    if (wxDirExists(targetPath))
    {
        return -3;
    }

    if (not wxRenameFile(sourcePath, targetPath))    
    {
        return -4;
    }

    return 0;
}


int MainFrame::backupCurrent()
{
    if (not currentGameAvailable())
    {
        return 0;
    }

    const wxDateTime date = wxDateTime::Now();
    const wxString tempName = date.Format("%Y%m%d_%H%M%S");

    int result = moveCurrentToGame(tempName); 

    if (result == 0)
    {
        backupName = tempName;
        log(wxT("Current game backed up as '" + tempName + wxT("'")));
    }
    else
    {
        log(wxT("ERROR: Could not backup the current game!"));
    }

    return result;
}


int MainFrame::restoreCurrent()
{
    if (backupName.IsEmpty())
    {
        return 0;
    }

    int result = moveGameToCurrent(backupName);

    if (result == 0)
    {
        log(wxT("Restored game '") + backupName + wxT("'"));
        backupName.Clear();
    }
    else
    {
        log(wxT("ERROR: Could not restore the backup!"));
    }

    return result;
}


int MainFrame::moveCurrentToGame(const wxString& targetGame)
{
    return moveDirectory(getSaveDir(CURRENT_GAME), getSaveDir(targetGame));
}


int MainFrame::moveGameToCurrent(const wxString& sourceGame)
{
    return moveDirectory(getSaveDir(sourceGame), getSaveDir(CURRENT_GAME));
}


void MainFrame::onStartNewClick(wxCommandEvent& event)
{
    if (isGameRunning())
    {
        wxMessageBox(wxT("Cannot start the game, it is already running"),
               wxT("Error"), wxICON_ERROR | wxOK);
        return;
    }

    wxString gameName = askGameName(wxT("new_game"));

    if (gameName.IsEmpty())
    {
        return;
    }

    backupCurrent();

    // Copy the current game to reuse the last settings
    if (not backupName.IsEmpty()
            && copyDirectory(getSaveDir(backupName), getSaveDir(CURRENT_GAME)) != 0)
    {
        wxMessageBox(wxT("Could not copy the files, aborting!"),
               wxT("Error"), wxICON_ERROR | wxOK);
    }
    else
    {
        log(wxT("Starting new game '") + gameName + wxT("'"));

        if (runGame())
        {
            wxMessageBox(wxT("Cannot start the game"), wxT("Error"), wxICON_ERROR | wxOK);
            log("ERROR: Could not start the game!");
        }
        else
        {
            moveCurrentToGame(gameName);
        }
    }

    restoreCurrent();
    refreshGames();
}


void MainFrame::onStartSelectedClick(wxCommandEvent& event)
{
    if (isGameRunning())
    {
        wxMessageBox(wxT("Cannot start the game, it is already running"),
               wxT("Error"), wxICON_ERROR | wxOK);
        return;
    }

    wxString gameName = selectedGame();

    if (gameName.IsEmpty())
    {
        return; // should not really happen
    }

    backupCurrent();
    moveGameToCurrent(gameName);
    log(wxT("Starting game '") + gameName + wxT("'"));

    if (runGame())
    {
        wxMessageBox(wxT("Cannot start the game"), wxT("Error"), wxICON_ERROR | wxOK);
        log(wxT("ERROR: Could not start the game!"));
    }

    moveCurrentToGame(gameName);
    restoreCurrent();
    refreshGames();
}


void MainFrame::onCopyClick(wxCommandEvent& event)
{
    wxString source = selectedGame();
    wxString target = askGameName(source + wxT("_copy"));

    if (target.IsEmpty())
    {
        return;
    }

    if (source == target)
    {
        wxMessageBox(wxT("Source and destination are the same"),
               wxT("Error"), wxICON_ERROR | wxOK);
        return;
    }

    if (copyDirectory(getSaveDir(source), getSaveDir(target)) != 0)
    {
        wxMessageBox(wxT("Could not copy the files"),
               wxT("Error"), wxICON_ERROR | wxOK);
    }

    refreshGames();
    log(wxT("Copied '") + source + ("' as '") + target + wxT("'"));
}


void MainFrame::onRenameClick(wxCommandEvent& event)
{
    wxString source = selectedGame();
    wxString target = askGameName(source);

    if (target.IsEmpty())
    {
        return;
    }

    if (source == target)
    {
        return;
    }

    if (moveDirectory(getSaveDir(source), getSaveDir(target)) != 0)
    {
        wxMessageBox(wxT("Could not rename the save directory"),
               wxT("Error"), wxICON_ERROR | wxOK);
    }

    refreshGames();
    log(wxT("Renamed '") + source + ("' to '") + target + wxT("'"));
    
}


void MainFrame::onDeleteClick(wxCommandEvent& event)
{
    wxString game = selectedGame();
    wxMessageDialog dlg(nullptr, wxT("Are you sure you want to delete '" + game + "'?"),
            wxT("Delete game"), wxYES_NO | wxNO_DEFAULT);

    if (dlg.ShowModal() == wxID_YES)
    {
        wxDir::Remove(getSaveDir(game), wxPATH_RMDIR_RECURSIVE);
        log(wxT("Deleted game '") + game + wxT("'"));
        refreshGames();
    }
}


void MainFrame::onOpenSaveDirClick(wxCommandEvent& event)
{
    if (wxExecute("explorer \"" + getSaveDirTop() + "\"") == 0)
    {
        // TODO err handling
    }
}


void MainFrame::onCloseClick(wxCommandEvent& event)
{
    Close(true);
}


void MainFrame::onGameSelected(wxCommandEvent& event)
{
    updateButtons();
}
